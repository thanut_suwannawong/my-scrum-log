import { EditProjectDialogComponent } from "./../../component/edit-project-dialog/edit-project-dialog.component";
import { ProjectService } from "./../../service/project.service";
import { Component, OnInit } from "@angular/core";
import { Project } from "src/app/model/project.model";
import { NewProjectInputComponent } from "src/app/component/new-project-dialog/new-project-input.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.css"],
})
export class ProjectComponent implements OnInit {
  dataSource: Array<Project>;
  displayedColumns: string[] = ["name", "code", "iManday", "pManday", "action"];

  constructor(
    private projetService: ProjectService,
    private dialog: MatDialog
  ) {
    this.dataSource = this.projetService.getProjectsList();
  }

  ngOnInit(): void {}

  onEdit(project: Project): void {
    this.dialog.open(EditProjectDialogComponent, {
      height: "280px",
      width: "400px",
      data: {
        name: project.name,
        code: project.code,
      },
    });
  }
  onDelete(): void {}
}
