import { ScrumService } from "../../../service/scrum.service";
import { Component, OnInit } from "@angular/core";
import { Scrum } from "src/app/model/scrum.model";

@Component({
  selector: "app-history",
  templateUrl: "./history.component.html",
  styleUrls: ["./history.component.css"],
})
export class HistoryComponent {
  scrumData: Array<Scrum>;

  constructor(private scrum: ScrumService) {
    this.scrumData = this.scrum.getScrumLog();
  }
}
