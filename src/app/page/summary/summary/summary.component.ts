import { ScrumService } from "src/app/service/scrum.service";
import { Project } from "src/app/model/project.model";
import { ProjectService } from "../../../service/project.service";
import { NewProjectInputComponent } from "../../../component/new-project-dialog/new-project-input.component";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Scrum } from "src/app/model/scrum.model";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-summary",
  templateUrl: "./summary.component.html",
  styleUrls: ["./summary.component.css"],
})
export class SummaryComponent implements OnInit, OnDestroy {
  projectData: Array<{ name: string; value: number }> = [];
  projectList: Array<Project> = [];
  hasValue: boolean = false;
  scrumData: Array<Scrum>;
  projectListSubcription: Subscription;

  constructor(
    private dialog: MatDialog,
    private projectService: ProjectService,
    private scrumService: ScrumService,
    private route: Router
  ) {
    this.scrumData = this.scrumService.getScrumLog();
    this.projectListSubcription = this.projectService.projectSubject.subscribe(
      (_) => this.getProjectList()
    );
  }

  ngOnInit(): void {
    this.getProjectList();
  }

  ngOnDestroy(): void {
    this.projectListSubcription.unsubscribe();
  }

  getProjectList() {
    this.projectList = this.projectService.getProjectsList();
    this.projectData = this.projectService
      .getProjectsList()
      .filter((project) => {
        return project.iMandays + project.pMandays > 0;
      })
      .map((val) => {
        return {
          name: val.code,
          value: val.iMandays + val.pMandays,
        };
      });
  }

  onNewProject() {
    this.dialog.open(NewProjectInputComponent, {
      height: "280px",
      width: "400px",
    });
  }
}
