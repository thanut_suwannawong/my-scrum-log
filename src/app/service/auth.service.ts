import { resolve } from "@angular/compiler-cli/src/ngtsc/file_system";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private userIsAdmin: boolean = true;

  constructor() {}

  public checkAuthed(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.userIsAdmin) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

  sudo() {
    this.userIsAdmin = !this.userIsAdmin;
    alert(
      this.userIsAdmin ? "sudo mode activated!!" : "sudo mode deactivated!!"
    );
  }
}
