import { Project } from "src/app/model/project.model";
import { Subject } from "rxjs";
export class ProjectService {
  private projects: Array<Project>;
  private projectKeys: string = "projects";
  projectSubject: Subject<any> = new Subject();

  constructor() {
    this.projects = JSON.parse(localStorage.getItem(this.projectKeys) || "[]");
  }

  getProjectsList(): Array<Project> {
    return this.projects;
  }

  addNewProject(data: Project): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projects.push(data);
        localStorage.setItem(this.projectKeys, JSON.stringify(this.projects));
        resolve(true);
      } catch (error) {
        console.error("Add a new project error :: ", error);
        reject(false);
      }
    });
  }

  editProject(oldName:string, oldCode:string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
       this.projects.find((ele) => ele.name == oldName)
        // this.projects[index] = data;
        localStorage.setItem(this.projectKeys, JSON.stringify(this.projects));
        resolve(true);
      } catch (error) {
        console.error("Edit project error :: ", error);
        reject(false);
      }
    });
  }

  deleteProject(index: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projects.splice(index, 1);
        localStorage.setItem(this.projectKeys, JSON.stringify(this.projects));
        resolve(true);
      } catch (error) {
        console.error("Delete project error :: ", error);
        reject(false);
      }
    });
  }

  plusManday(projectCode: string, projectTaskType: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projects.forEach((project) => {
          if (project.code == projectCode) {
            if (projectTaskType == 1) {
              project.iMandays += 1;
            } else {
              project.pMandays += 1;
            }
          }
        });
        localStorage.setItem(this.projectKeys, JSON.stringify(this.projects));
        resolve(true);
      } catch (error) {
        console.error("Plus manday error :: ", error);
        reject(false);
      }
    });
  }

  subtractManday(
    projectCode: string,
    projectTaskType: number
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projects.forEach((project) => {
          if (project.code == projectCode) {
            if (projectTaskType == 1) {
              project.iMandays -= 1;
            } else {
              project.pMandays -= 1;
            }
          }
        });
        localStorage.setItem(this.projectKeys, JSON.stringify(this.projects));
        resolve(true);
      } catch (error) {
        console.error("Subtract manday error :: ", error);
        reject(false);
      }
    });
  }

  transferManday(
    subtractProjectCode: string,
    plusProjectCode: string
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projects.forEach((project) => {
          if (project.code == subtractProjectCode) {
            project.iMandays -= 1;
          }
          if (project.code == plusProjectCode) {
            project.iMandays += 1;
          }
        });
        localStorage.setItem(this.projectKeys, JSON.stringify(this.projects));
        resolve(true);
      } catch (error) {
        console.error("Subtract manday error :: ", error);
        reject(false);
      }
    });
  }

  transferMandayToAnotherType(
    projectCode: string,
    oldTaskType: number
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projects.forEach((project) => {
          // Task type 1 : Implement
          // Task type 2 : Presale
          if (project.code == projectCode) {
            if (oldTaskType == 1) {
              project.iMandays -= 1;
              project.pMandays += 1;
            } else {
              project.pMandays -= 1;
              project.iMandays += 1;
            }
          }
        });
        localStorage.setItem(this.projectKeys, JSON.stringify(this.projects));
        resolve(true);
      } catch (error) {
        console.error("Subtract manday error :: ", error);
        reject(false);
      }
    });
  }
}
