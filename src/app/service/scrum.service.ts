import { ProjectService } from "./project.service";
import { Scrum } from "src/app/model/scrum.model";
import { Injectable } from "@angular/core";
import { Project } from "../model/project.model";
@Injectable({
  providedIn: "root",
})
export class ScrumService {
  private scrums: Array<Scrum>;
  private scrumsKey: string = "scrums";

  constructor(private projectService: ProjectService) {
    this.scrums = JSON.parse(localStorage.getItem(this.scrumsKey) || "[]");
  }

  getScrumLog(): Array<Scrum> {
    return this.scrums.sort(
      (a, b) => <any>new Date(b.date) - <any>new Date(a.date)
    );
  }

  addNewRecord(data: Scrum): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projectService
          .plusManday(data.project, data.type)
          .then(() => {
            this.scrums.push(data);
            localStorage.setItem(this.scrumsKey, JSON.stringify(this.scrums));
            resolve(true);
          })
          .catch((_) => reject(false));
      } catch (error) {
        console.error("Add new record error :: ", error);
        reject(false);
      }
    });
  }

  editRecord(index: number, data: Scrum): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        let isSameProject = this.scrums[index].project == data.project;
        let isSameType = this.scrums[index].type == data.type;
        if (!isSameProject) {
          this.projectService
            .transferManday(this.scrums[index].project, data.project)
            .catch(() => reject(false));
        }
        if (!isSameType) {
          this.projectService
            .transferMandayToAnotherType(
              this.scrums[index].project,
              this.scrums[index].type
            )
            .catch(() => reject(false));
        }
        this.scrums[index] = data;
        localStorage.setItem(this.scrumsKey, JSON.stringify(this.scrums));
        resolve(true);
      } catch (error) {
        console.error("Edit record error :: ", error);
        reject(false);
      }
    });
  }

  deleteRecord(index: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        this.projectService
          .subtractManday(this.scrums[index].project, this.scrums[index].type)
          .then(() => {
            this.scrums.splice(index, 1);
            localStorage.setItem(this.scrumsKey, JSON.stringify(this.scrums));
            resolve(true);
          })
          .catch(() => reject(false));
      } catch (error) {
        console.error("Delete record error :: ", error);
        reject(false);
      }
    });
  }
}
