import { AuthService } from './service/auth.service';
import { Component } from '@angular/core';
import { environment } from './../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-scrum-log';
  constructor(
    private authService:AuthService
  ){
    console.log("API URL :: ",environment.apiUrl);
    
  }

  onSudo(){
    this.authService.sudo();
  }
}
