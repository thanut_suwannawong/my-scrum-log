import { ConfirmDialogComponent } from "./../confirm-dialog/confirm-dialog.component";
import { ProjectService } from "./../../service/project.service";
import { ScrumService } from "./../../service/scrum.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Inject } from "@angular/core";
import { Component } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { Scrum } from "src/app/model/scrum.model";
import { Project } from "src/app/model/project.model";

@Component({
  selector: "app-edit-dialog",
  templateUrl: "./edit-dialog.component.html",
  styleUrls: ["./edit-dialog.component.css"],
})
export class EditDialogComponent {
  scrumForm: FormGroup;
  index: number;
  projectsList: Array<Project>;

  constructor(
    private dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { scrum: Scrum; index: number },
    private scrumService: ScrumService,
    private projectService: ProjectService,
    private matDialog: MatDialog
  ) {
    this.scrumForm = new FormGroup({
      date: new FormControl(data.scrum.date, [Validators.required]),
      type: new FormControl(data.scrum.type, [Validators.required]),
      project: new FormControl(data.scrum.project, [Validators.required]),
      description: new FormControl(data.scrum.description, [
        Validators.required,
      ]),
    });
    this.index = data.index;
    this.projectsList = this.projectService.getProjectsList();
  }

  onSubmit() {
    this.matDialog.open(ConfirmDialogComponent, {
      width: '300px',
      height: '150px',
      data: {
        dialogText: "Are you sure?",
        dialogRightBtnCallback: () => this.editData(),
      },
    });
  }

  editData() {
    let newData: Scrum = new Scrum(
      this.scrumForm.get("date")?.value,
      this.scrumForm.get("type")?.value,
      this.scrumForm.get("project")?.value,
      this.scrumForm.get("description")?.value
    );
    this.scrumService
      .editRecord(this.index, newData)
      .then((_) => alert("Edit success!"))
      .catch((_) => alert("Failed to edit!"))
      .finally(() => this.dialogRef.close());
  }
}
