import { DeleteConfirmDialogComponent } from "./../delete-confirm-dialog/delete-confirm-dialog.component";
import { EditDialogComponent } from "./../edit-dialog/edit-dialog.component";
import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Scrum } from "src/app/model/scrum.model";
@Component({
  selector: "record-card",
  templateUrl: "./record-card.component.html",
  styleUrls: ["./record-card.component.css"],
})
export class RecordCardComponent {
  @Input() data!: Scrum;
  @Input() index!: number;

  constructor(private dialog: MatDialog) {}

  onEdit() {  
    this.dialog.open(EditDialogComponent, {
      height: "400px",
      width: "550px",
      data: { scrum: this.data, index: this.index },
    });
  }

  onDelete() {
    this.dialog.open(DeleteConfirmDialogComponent, {
      height: "200px",
      width: "400px",
      data: { scrum: this.data, index: this.index },
    });
  }
}
