import { Component, Inject, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Scrum } from "src/app/model/scrum.model";
import { ScrumService } from "src/app/service/scrum.service";
import { EditDialogComponent } from "../edit-dialog/edit-dialog.component";

@Component({
  selector: "app-delete-confirm-dialog",
  templateUrl: "./delete-confirm-dialog.component.html",
  styleUrls: ["./delete-confirm-dialog.component.css"],
})
export class DeleteConfirmDialogComponent {
  scrumData: Scrum;
  index: number;

  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { scrum: Scrum; index: number },
    private scrumService: ScrumService
  ) {
    this.scrumData = data.scrum;
    this.index = data.index;
  }

  onDelete() {
    this.scrumService
      .deleteRecord(this.index)
      .then((_) => alert("Delete success!"))
      .catch((_) => alert("Failed to delete!"))
      .finally(() => this.dialogRef.close());
  }

  onCancel() {
    this.dialogRef.close();
  }
}
