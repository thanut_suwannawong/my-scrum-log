import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRecordInputComponent } from './new-record-input.component';

describe('NewRecordInputComponent', () => {
  let component: NewRecordInputComponent;
  let fixture: ComponentFixture<NewRecordInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewRecordInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRecordInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
