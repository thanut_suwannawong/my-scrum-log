import { ProjectService } from './../../service/project.service';
import { Project } from './../../model/project.model';
import { ScrumService } from "./../../service/scrum.service";
import { Scrum } from "./../../model/scrum.model";

import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-new-record-input",
  templateUrl: "./new-record-input.component.html",
  styleUrls: ["./new-record-input.component.css"],
})
export class NewRecordInputComponent {
  scrumForm: FormGroup;
  projectsList: Array<Project>;

  constructor(private scrumService: ScrumService,
    private projectService: ProjectService) {
    this.scrumForm = new FormGroup({
      date: new FormControl(null, [Validators.required]),
      type: new FormControl(null, [Validators.required]),
      project: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
    });
    this.projectsList = this.projectService.getProjectsList();
  }

  onSubmit() {
    this.scrumService
      .addNewRecord(
        new Scrum(
          this.scrumForm.get("date")?.value,
          this.scrumForm.get("type")?.value,
          this.scrumForm.get("project")?.value,
          this.scrumForm.get("description")?.value
        )
      )
      .then((_) => {
        alert("Success!");
      })
      .catch((_) => {
        alert("Failed to add a new record!");
      });
    this.scrumForm.reset();
  }
}
