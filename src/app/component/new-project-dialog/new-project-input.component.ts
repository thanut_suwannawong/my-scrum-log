import { ProjectService } from "../../service/project.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Project } from "src/app/model/project.model";
import { MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: "app-new-project-input",
  templateUrl: "./new-project-input.component.html",
  styleUrls: ["./new-project-input.component.css"],
})
export class NewProjectInputComponent {
  newProjectFormGroup: FormGroup;

  constructor(
    private projectService: ProjectService,
    public dialogRef: MatDialogRef<NewProjectInputComponent>
  ) {
    this.newProjectFormGroup = new FormGroup({
      name: new FormControl(null, Validators.required),
      code: new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    let name = this.newProjectFormGroup.get("name")?.value;
    let code = this.newProjectFormGroup.get("code")?.value;
    let data: Project = new Project(name, code, 0, 0);
    this.projectService
      .addNewProject(data)
      .then((_) => {
        alert("Success!");
        this.projectService.projectSubject.next();
      })
      .catch((_) => {
        alert("Failed to add a new record!");
      });
    this.newProjectFormGroup.reset();
  }
}
