import { ProjectService } from "./../../service/project.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Scrum } from "src/app/model/scrum.model";
import { Project } from "src/app/model/project.model";

@Component({
  selector: "app-edit-project-dialog",
  templateUrl: "./edit-project-dialog.component.html",
  styleUrls: ["./edit-project-dialog.component.css"],
})
export class EditProjectDialogComponent implements OnInit {
  newProjectFormGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditProjectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { name: string; code: string },
    private projectService: ProjectService
  ) {
    this.newProjectFormGroup = new FormGroup({
      name: new FormControl(this.data.name, Validators.required),
      code: new FormControl(this.data.code, Validators.required),
    });
  }

  ngOnInit(): void {}

  onSubmit(): void {
    // this.projectService.editProject(
    // );
  }
}
