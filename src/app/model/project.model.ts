export class Project {
  constructor(
    public name: string,
    public code: string,
    public iMandays: number,
    public pMandays: number
  ) {}
}
