import { Project } from './project.model';
export class Scrum {
    constructor(
        public date: string,
        public type: number,
        public project: string,
        public description: string) { }
}