import { SummaryComponent } from "./page/summary/summary/summary.component";

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NewRecordComponent } from "./page/new_record/new-record/new-record.component";
import { HistoryComponent } from "./page/history/history/history.component";
import { ProjectComponent } from "./page/project/project.component";
import { AuthGuard } from "./guard/auth.guard";

const routes: Routes = [
  { path: "new_record", component: NewRecordComponent },
  { path: "summary", component: SummaryComponent },
  { path: "history", component: HistoryComponent },
  { path: "project", component: ProjectComponent, canActivate:[AuthGuard] },
  { path: "**", redirectTo: "summary" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
