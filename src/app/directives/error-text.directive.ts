import { ElementRef } from '@angular/core';
import { Directive } from '@angular/core';

@Directive({
  selector: '[errorText]'
})
export class ErrorTextDirective {

  constructor(el: ElementRef) { 
    el.nativeElement.style.color = 'red';
  }
}
