import { MandaysDescriptionPipe } from "./pipe/mandays-description.pipe";
import { ProjectService } from "./service/project.service";
import { ScrumService } from "./service/scrum.service";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material/list";
import { MatButtonModule } from "@angular/material/button";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { NewRecordComponent } from "./page/new_record/new-record/new-record.component";
import { SummaryComponent } from "./page/summary/summary/summary.component";
import { HistoryComponent } from "./page/history/history/history.component";
import { NewRecordInputComponent } from "./component/new-record-input/new-record-input.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ReactiveFormsModule } from "@angular/forms";
import { ErrorTextDirective } from "./directives/error-text.directive";
import { RecordCardComponent } from "./component/record-card/record-card.component";
import { MatDialogModule } from "@angular/material/dialog";
import { EditDialogComponent } from "./component/edit-dialog/edit-dialog.component";
import { DeleteConfirmDialogComponent } from "./component/delete-confirm-dialog/delete-confirm-dialog.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { NewProjectInputComponent } from "./component/new-project-dialog/new-project-input.component";
import { ConfirmDialogComponent } from "./component/confirm-dialog/confirm-dialog.component";
import { ProjectComponent } from "./page/project/project.component";
import { MatTableModule } from "@angular/material/table";
import { EditProjectDialogComponent } from './component/edit-project-dialog/edit-project-dialog.component';
@NgModule({
  declarations: [
    AppComponent,
    NewRecordComponent,
    SummaryComponent,
    HistoryComponent,
    NewRecordInputComponent,
    ErrorTextDirective,
    RecordCardComponent,
    EditDialogComponent,
    DeleteConfirmDialogComponent,
    NewProjectInputComponent,
    MandaysDescriptionPipe,
    ConfirmDialogComponent,
    ProjectComponent,
    EditProjectDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    NgbModule,
    ReactiveFormsModule,
    MatDialogModule,
    NgxChartsModule,
    MatTableModule
  ],
  providers: [ScrumService, ProjectService],
  bootstrap: [AppComponent],
})
export class AppModule {}
