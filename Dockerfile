FROM nginxinc/nginx-unprivileged
COPY default.conf /etc/nginx/conf.d/default.conf
COPY dist/my-scrum-log /usr/share/nginx/html